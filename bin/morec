#!/usr/bin/env node

var morec = require('../'),
    os = require('os'),
    path = require('path'),
    program = require('commander');

function includePath(val) {
  return val.split(os.type().match(/Windows/) ? ';' : ':').map(function (p) {
    if (p) {
      return path.resolve(process.cwd(), p);
    }
  });
}

function optimization(val) {
  return parseInt(val, 10);
}

function lineNumbers(val) {
  if (val === 'comments' || val === 'mediaquery' || val === 'all') {
    return val;
  }
  return '';
}

function fileLimit(val) {
  return parseInt(val, 10);
}

program
  .version('0.2.5')
  .usage('[options] <input> <output>')
  .option('--include-path <path>', 'Set include paths. Separated by \':\'. Use ' +
                                   '\';\' on Windows.', includePath)
  .option('--no-color', 'Disable colorized output.')
  .option('-s, --silent', 'Suppress output of error messages.')
  .option('--ignore-errors', 'Ignore parsing errors and continue compilation.')
  .option('--strict-imports', 'Force evaluation of imports.')
  .option('--verbose', 'Be verbose.')
  .option('-x, --compress', 'Compress output by removing some whitespaces.')
  .option('--yui-compress', 'Compress output using cssmin.js')
  .option('-O <level>', 'Set the parser\'s optimization level. The lower the ' +
                        'number, the less nodes it will create in the tree. ' +
                        'This could matter for debugging, or if you want to ' +
                        'access the individual nodes in the tree.', optimization, 1)
  .option('--line-numbers <type>', 'Outputs filename and line numbers. Type can ' +
                                   'be either \'comments\', which will output ' +
                                   'the debug info within comments, ' +
                                   '\'mediaquery\' that will output the ' +
                                   'information within a fake media query which ' +
                                   'is compatible with the SASS format, and ' +
                                   '\'all\' which will do both.', lineNumbers)
  .option('--file-limit <limit>', 'Limit the number of files open concurrently', fileLimit)
  .parse(process.argv);

var input, output;
if (program.args[0] && program.args[1]) {
  input = path.resolve(process.cwd(), program.args[0]);
  output = path.resolve(process.cwd(), program.args[1]);
} else if (program.args[0]) {
  input = output = path.resolve(process.cwd(), program.args[0]);
} else {
  input = output = process.cwd();
}

var options = {
  ignoreErrors: program.ignoreErrors || false,
  silent: program.silent || false,
  strictImports: program.strictImports || false,
  paths: program.includePath || [],
  verbose: program.verbose || false,
  compress: program.compress || false,
  yuicompress: program.yuiCompress || false,
  optimization: program.O,
  color: program.color,
  dumpLineNumbers: program.lineNumbers || '',
  fileLimit: program.fileLimit || 10
};

morec.compileDirectory(input, output, options);

