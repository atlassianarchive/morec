var Bagpipe = require('bagpipe'),
    events = require('events'),
    fs = require('fs'),
    less = require('less'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    readdirp = require('readdirp');

var morec = module.exports = new events.EventEmitter();

var printError = function (err) {
  morec.filesQueued--;

  if (!morec.options.silent) {
    console.error(err);
  }

  if (!morec.options.ignoreErrors) {
    process.exit(1);
  }
  morec.exitCode = 1;
};

var compileFile = function (file) {
  morec.readQueue.push(fs.readFile, file, 'utf-8', function (err, data) {
    if (err) {
      printError(err);
    }

    new less.Parser({
      paths: [path.dirname(file)].concat(morec.options.paths),
      optimization: morec.options.optimization,
      filename: file,
      strictImports: morec.options.strictImports,
      dumpLineNumbers: morec.options.dumpLineNumbers
    }).parse(data, function (err, tree) {
      if (err) {
        printError(less.formatError(err, morec.options));
      }

      var outputPath = path.join(morec.output, file.replace(morec.input, '')
                                                   .replace('.less', '.css'));

      mkdirp(path.dirname(outputPath), function (err) {
        if (err) {
          printError(err);
        }

        var source;
        try {
          source = tree.toCSS({
            compress: morec.options.compress,
            yuicompress: morec.options.yuicompress
          });
        } catch (e) {
          printError(less.formatError(e, morec.options));
        }

        if (source) {
          morec.writeQueue.push(fs.writeFile, outputPath, source, 'utf-8', function (err) {
            if (err) {
              printError(err);
            }

            morec.emit('compiled', {
              input: file,
              output: outputPath
            });
          });
        } else {
          morec.filesQueued--;
        }
      });
    });
  });
};

morec.compileDirectory = function (input, output, options) {
  this.input = input;
  this.output = output;
  this.options = options;
  this.exitCode = 0;
  this.filesQueued = 0;
  this.filesCompiled = 0;

  var self = this,
      files = [];

  this.readQueue = new Bagpipe(morec.options.fileLimit);
  this.writeQueue = new Bagpipe(morec.options.fileLimit);

  readdirp({
    root: input,
    fileFilter: '*.less'
  })
    .on('data', function (file) {
      self.filesQueued++;
      files.push(file.fullPath);
    })
    .on('end', function () {
      files.forEach(function (file) {
        compileFile(file);
      });
    });

  this.on('compiled', function (data) {
    self.filesCompiled++;

    if (self.options.verbose) {
      console.log("compiled '%s' to '%s'", data.input, data.output);
    }

    if (self.filesQueued === self.filesCompiled) {
      process.exit(code=self.exitCode);
    }
  });
};

