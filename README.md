# morec

`morec` compiles all LESS files in a directory, maintaining the existing folder
and file structure. It is largely compatible with the `lessc` option syntax, with
some minor differences for consistency's sake.

## Installation

```
npm install -g morec
```

## Usage

```
morec [options] <input> <output>

Options:

  -h, --help             output usage information
  -V, --version          output the version number
  --include-path <path>  Set include paths. Separated by ':'. Use ';' on Windows.
  --no-color             Disable colorized output.
  -s, --silent           Suppress output of error messages.
  --ignore-errors        Ignore parsing errors and continue compilation.
  --strict-imports       Force evaluation of imports.
  --verbose              Be verbose.
  -x, --compress         Compress output by removing some whitespaces.
  --yui-compress         Compress output using cssmin.js
  -O <level>             Set the parser's optimization level. The lower the
                         number, the less nodes it will create in the tree. This
                         could matter for debugging, or if you want to access the
                         individual nodes in the tree.
  --line-numbers <type>  Outputs filename and line numbers. Type can be either
                         'comments', which will output the debug info within
                         comments, 'mediaquery' that will output the information
                         within a fake media query which is compatible with the
                         SASS format, and 'all' which will do both.
  --file-limit <limit>   Limit the number of files open concurrently
```

If you do not provide an `output` argument, `morec` will write the compiled
files to the same location as the source files.

If you do not provide an `input` *or* `output` argument, `morec` will compile all
LESS files in the current working directory to the same location as the source
files.

## License

Apache 2.0. See LICENSE for details.

## Authors

Jon Mooring <jmooring@atlassian.com>

